from django.urls import path

from . import views

# I don't want anyone creating an account, so I added 'bla' to path and deleted
# signup from name, in addition to commenting out the html in signup.html

urlpatterns = [
    path('signupbla/', views.SignUpView.as_view(), name=''),
]