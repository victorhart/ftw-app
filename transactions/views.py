from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from .models import Transaction

class HomePageView(ListView):
    model = Transaction
    template_name = 'home.html'


class TransactionDetailView(DetailView):
    model = Transaction
    template_name = 'transaction_detail.html'


class TransactionCreateView(CreateView):
    model = Transaction
    template_name = 'transaction_new.html'
    fields = '__all__'

class TransactionUpdateView(UpdateView):
    model = Transaction
    template_name = 'transaction_edit.html'
    fields = '__all__'

class TransactionDeleteView(DeleteView):
    model = Transaction
    template_name = 'transaction_delete.html'
    success_url = reverse_lazy('home')