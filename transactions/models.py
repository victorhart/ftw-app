from django.db import models
from datetime import date
from django.urls import reverse

# Create your models here.

class Transaction(models.Model):
    ACCOUNT_CHOICES = (('EV', 'efetivo Victor'), ('EC', 'efetivo Cindy'), ('16', '16894-7'),
                       ('18', '189-9'), ('EM', 'efetivo Mocinha'), ('66', '6656 - VISA'),
                       ('05', '0502 - VISA'))
    description = models.CharField(max_length=500, verbose_name = 'Descrição')
    date = models.DateField(default=date.today, verbose_name = 'Data')
    account = models.CharField(max_length=50, choices=ACCOUNT_CHOICES, verbose_name = 'Conta')
    credit = models.DecimalField(max_digits=7, default=0, decimal_places=2, verbose_name='Crédito')
    debit = models.DecimalField(max_digits=7, decimal_places=2, verbose_name='Débito')
    category = models.CharField(max_length=50, default='gastos', verbose_name = 'Categoria')

    def __str__(self):
        """A string representation of the model."""
        return self.description[:50]

    def get_absolute_url(self):
        return reverse('transaction_detail', args=[str(self.id)])
